<?php

class MyClass {
//	private $arr = [];
	private $name, $age;

	function __toString() {
		return 'Это обьект ' . __CLASS__;
	}
	function __set($n, $v) {
		switch($n) {
			case 'name': $this->name = $v; break;
			case 'age': $this->age = $v; break;
			default: throw new Exception('!!!');
		}
	}
	function __get($n) {
		return $this->$n;
	}
}

$obj = new MyClass();
echo $obj;
