<?php
class SuperClass {
	function functionName() {
		echo '<p>Вызвана функция ' . __FUNCTION__ . '</p>';
	}
	function className() {
		echo '<p>Используется класс ' . __CLASS__ . '</p>';
	}
	function methodName() {
		echo '<p>Вызван метод ' . __METHOD__ . '</p>';
	}
}
$obj = new SuperClass();

$obj->functionName();   // functionName
$obj->className();      // SuperClass
$obj->methodName();     // SuperClass::methodName


// Перегрузка метода при наследовании нужно просто создать метод в новом классе с таким же названием.
// у дочерних классов, запускается сначала проверка есть ли у класса метод, если нет то ищет у родителя.
// Вызов метод у родительского класса parent::build();




//обработка исключений
function test($var = false){
	try {
		echo "Start\n";
		if(!$var) {
			// если $var пустая тогда появляется ошибка
			// из throw перебрасывает в блок catch
			throw new Exception('$var is false!');
		}

		echo "Continue\n";
	} catch(Exception $e){
		// то что мы указали в new Exception('$var is false!');
		echo "Exception: " . $e->getMessage() . "\n";
		// название файла
		echo "in file: " . $e->getFile() . "\n";
		// номер строки
		echo "on line: " . $e->getLine() . "\n";
	}

	echo "The end\n";
}
// Генерируем собственное исключение
// throw new MathException("На 0 делить нельзя!");
// Генерируем встроенное исключение
// throw new Exception("Что-то случилось!");

// catch(MathException $e);
// catch(Exception $e);
// всегда записываем в конец, тк MathException наследует Exception и может провалиться в catch(Exception $e).

// abstract класс, невозможно создать экземпляр
// abstract метод не имеет реализации, реализация у наследуемых классов


// Интерфейс - (interface) реализует только абстрактные методы и константы.
interface Painteble {
	function paint();
}
// Класс может реализовать(implement) сколько угодно интерфейсов,
// но у них не должно быть одинаковых методов с разными сигнатурами.
// instanceOf - проверка на наследование.
if ($car instanceOf Painteble) {
	$car->paint();
}

// Константы класса.
const VARIABLE = 10;
// Обращаться внутри класса
self::color;
// Снаружи класса
ClassName::color;
// ТК константа принадлежит классу,а не переменной.

// Статические поля класса
public static $variable = 0;
// Обращение внутри
self::$variable;
// Обращение снаружи
ClassName::$variable;
// Пример
function __construct($name) {
	++self::$variable;
}

// Статические методы класса
static function welcome() {
	echo 'hello';
}
// Вызов метода
ClassName::welcome();

// магическая функция при использовании класса автоматически загружает
function __autoload($name) {
	require "$name.class.php";
}

// Модификаторы доступа
// private - не доступна в наследниках.
// protected - доступен наследнику

function __set($name, $value) {
	// установка поля класса которого нет в классе, тоесть динамическое создание полей
}
function __get($name) {
	// получение поля класса которого нет в классе
}


class MyClass {
	private $arr = [];
	function __set($n, $v) {
		$this->arr[$n] = $v;
	}
	function __get($n) {
		return $this->arr[$n];
	}
}

$obj = new MyClass();
$obj->param = 100;
echo $obj->param;


// Замена работы с полем с public на private
//////////////////////
// пример на public //
//////////////////////
class Human {
	public $name;
}
$man = new Human();
$man->name = 'DMA';
/////////////////////////
// пример на protected //
/////////////////////////
class Human {
	private $name;
	function __set($name, $value) {
		switch ($name) {
			case 'name': $this->name = $value; break;
		}
	}
	
	// вывод обьекта как строку
	function __toString() {
		return 'Это обьект ' . __CLASS__;
	}
}
$man = new Human();
$man->name = 'DMA';
echo $man;	// Это обьект Human

//////////////////////////////////
// вызов несуществующего метода //
//////////////////////////////////
function __call($name, $args) {
	echo 'Вызываем метод ' . $name . ' с аргументами ' . implode(', ', $args);
}
////////////////////////////////////////////////
// вызов несуществующего статического метода //
//////////////////////////////////////////////
function __callStatic($name, $args) {
	echo 'Вызываем статического метода ' . $name . ' с аргументами ' . implode(', ', $args);
}
////////////////////////////////////////
// Обращение к обьекту как к функции //
//////////////////////////////////////
class Math {
	function __invoke( $num, $action ) {
		switch ( $action ) {
			case '+':
				return $num + $num;
			case '*':
				return $num * $num;
			default:
				throw new Exception( 'Неизвестная операция' );
		}
	}
}
// вызов
$obj = new Math();
echo $obj(5, '+');  // 10
echo $obj(5, '*');  // 25


///////////////////////////
// Сериализация объекта //
/////////////////////////
// Вызывается перед сериализацией
function __sleep(){
    return ['login', 'password'];
}
// Вызывается после сериализации
function __wakeup(){
	$this->params = $this->getUser();
}

////////////////////////////////
// Финальные классы и методы //
//////////////////////////////
//нельзя наследовать и переопределать
final class Math{
	final function sum() {
		return;
	}
}

/////////////
// ТРЕЙТЫ //
///////////
trait Hello {
	private function sayHello($name) {
		return "Hello, $name!";
	}
}
// использование трейтов в классе
class Welcome {
	use Hello {
		sayHello as public;
	}
}




// Разрешение конфликтов имён
trait Hello {
	private function sayHello() {
		return "Hello";
	}
}
trait User {
	public function sayHello( $name ) {
		return $name;
	}
}
class Welcome {
	use User, Hello {
		Hello::sayHello as public word;
		User::sayHello insteadof Hello;
	}
}
$obj = new Welcome();
echo $obj->word(), ', ', $obj->sayHello( 'John' ), '!';



//////////////////////////////////////
// Ожидается то, что можно вызвать //
////////////////////////////////////
function foo( callable $x ) {
	if ( func_num_args() == 2 ) {
		$m = func_get_arg( 1 );

		return $x->$m();
	} elseif ( is_array( $x ) ) {
		return $x[0]::$m[1]();
	} else {
		return $x();
	}
}
echo foo( $obj, "func" ); // MyClass::func, вызывает метод класса
echo foo( [ "MyClass", "staticFunc" ] ); // MyClass::staticFunc, вызывает статический метод класса
echo foo( $obj ); // MyClass::__invoke, вызывается магический метод инвоке если определен.

/////////////
// SQLite //
///////////

$db = new SQLite3( "test.db" );     // Создаём или открываем базу данных test.db
$db->close();       // Закрываем базу данных без удаления объекта
$db->open( "another.db" );      // Открываем другую базу данных для работы
unset( $db );       // Удаляем объект
