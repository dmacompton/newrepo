<?php

class User extends UserAbstract {
	public $name;
	public $login;
	public $password;
	public static $counter = 0;

	function __construct( $n, $l, $p ) {
		$this->name     = $n;
		$this->login    = $l;
		$this->password = $p;
		++ self::$counter;
	}

	function __destruct() {
		echo "<br>Object deleted";
	}

	function __clone() {
		echo "<br>Object cloned<br>";
	}

	function showInfo() {
		echo "<hr>Name: {$this->name}<br>";
		echo "Login: {$this->login}<br>";
		echo "Pass: {$this->password}<br>";
	}
}
