<?php

class SuperUser extends User implements ISuperUser {
	private $role;
	public static $counter = 0;

	function __construct( $n, $l, $p, $r ) {
		parent::__construct( $n, $l, $p );
		$this->role = $r;
		++ self::$counter;
	}

	function showInfo() {
		parent::showInfo();
		echo "Role: {$this->role}<br>";
	}

	function getInfo() {
		return array(
			'name'     => $this->name,
			'login'    => $this->login,
			'password' => $this->password,
			'role'     => $this->role
		);
	}
}
