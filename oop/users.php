<?php
function __autoload($name) {
	require "classes/$name.class.php";
}

$user1 = new User('John Smith', 'johns', '1234');
$user1->showInfo();

$user2 = new User('Mike Dow', 'miked', '5678');
$user2->showInfo();

$user3 = new User('Ivan Petrov', 'ivanp', '9632');
$user3->showInfo();

$user4 = clone $user3;

$user = new SuperUser('Vasya Pupkin', 'vasyap', '0000', 'admin');
$user->showInfo();
var_dump($user->getInfo());


echo 'Всего обычных пользователей: '.User::$counter;
echo '<br>Всего супер-пользователей: '.SuperUser::$counter;